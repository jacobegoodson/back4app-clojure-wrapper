(ns back4app-clojure-wrapper.core
  (:require [cheshire.core :as json]
            [clj-http.client :as client]))

;;generate random password
(defn generate-random-password
  []
  (apply str (for [_ (range 20)]
               (rand-nth "0123456789abcdefghijklmnopqrstuvwxyz_!@"))))


(def application-id "UYIEn9KoNDtjscFGuqTrTopKJdJsuGMfWAUooNN7")
(def rest-api-key "BgtjLaDpKMp7DVigGnfvogtdaTHqqfD3QQILS6zT")


(def url "https://parseapi.back4app.com/classes/")


;; get example => url, appId, REST-API-KEY

(client/get "https://parseapi.back4app.com/classes/Student"
            {:headers {"X-Parse-Application-Id" application-id
                       "X-Parse-REST-API-Key" rest-api-key}})

(client/post "https://parseapi.back4app.com/classes/Student"
             {:headers {"X-Parse-Application-Id" application-id
                        "X-Parse-REST-API-Key" rest-api-key
                        "Content-Type" "application/json"}
              :body (json/generate-string {:username "lickymyclicky" (generate-random-password)})})

(client/put "https://parseapi.back4app.com/classes/Student/6fWuKi2HBJ"
             {:headers {"X-Parse-Application-Id" application-id
                        "X-Parse-REST-API-Key" rest-api-key
                        "Content-Type" "application/json"}
              :body (json/generate-string {:username "Bear"})})

(client/delete "https://parseapi.back4app.com/classes/Student/PM38MSJEtX"
               {:headers {"X-Parse-Application-Id" application-id
                          "X-Parse-REST-API-Key" rest-api-key}})

(defn keyify-map
  [mapp]
  (apply hash-map
         (flatten (map (fn [[key val]] [(keyword key) val])
                       mapp))))

(defn to-clojure
  [json-str]
  (map keyify-map
       ((json/parse-string (:body json-str)) "results")))

;;Get fetch
(defn get
  [class]
  (to-clojure
    (client/get (str url class)
                {:headers {"X-Parse-Application-Id" application-id
                           "X-Parse-REST-API-Key" rest-api-key}})))

;;Post Create
(defn post
  [class init]
  (client/post (str url class)
               {:headers {"X-Parse-Application-Id" application-id
                          "X-Parse-REST-API-Key" rest-api-key
                          "Content-Type" "application/json"}
                :body (json/generate-string init)}))

;;Put Update
(defn put
  [class init]
  (client/put (str url class)
              {:headers {"X-Parse-Application-Id" application-id
                          "X-Parse-REST-API-Key" rest-api-key
                          "Content-Type" "application/json"}
               :body (json/generate-string init)}))

;;Delete
(defn delete
  [class]
  (client/delete (str url class)
                 {:headers {"X-Parse-Application-Id" application-id
                            "X-Parse-REST-API-Key" rest-api-key}}))

(defn delete-student-by-name
  [name]
  (delete (str "Student/"
               (:objectId (first (filter (fn [student] (= (:username student) name))
                                         (get "Student")))))))

;;Work it!!!
(get "Student")
(post "Student" {:username "Jacob Goodone" :password (generate-random-password)})
(put "Student/6fWuKi2HBJ" {:username "Bears"})
(delete-student-by-name "Adam Ingram")

